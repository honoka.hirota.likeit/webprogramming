package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	//ログインIDとパスワードに紐づくユーザ情報を返す

	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			//主キーに紐づくレコードは１件のみなので、rs.next()は一回だけ行う
			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			//データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			//管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user  WHERE login_id NOT IN ('admin')";

			//SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			//結果表に格納されたレコードの内容をUserインスタンスに設定し、ArrayListに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}

	public  List<User> Search(String loginId, String name, String birthDate1, String birthDate2) {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			//データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			//管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id NOT IN ('admin')";

			if(!loginId.equals("")) {
				sql+=" AND login_id = '"+loginId+"'";
			}

			if(!name.equals("")) {
				sql+=" AND name = '"+name+"'";
			}

			if(!birthDate1.equals("") && !birthDate2.equals("")) {
				sql+=" AND birth_date BETWEEN '"+birthDate1 +"' AND '"+birthDate2+"'";
			}

			//SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			System.out.println(sql);

			ResultSet rs = stmt.executeQuery(sql);

			//ログイン成功時の処理
			while (rs.next()) {
				int id = rs.getInt("id");
				loginId = rs.getString("login_id");
				name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


	public void userRegistration(String loginId, String password, String name, String birthDate) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERT文を準備
			String sql = "INSERT INTO user (login_id, password, name, birth_date, create_date, update_date) VALUES (?, ?, ?, ?, now(), now())";

			// INSERTを実行
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, password);
			stmt.setString(3, name);
			stmt.setString(4, birthDate);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User Registered(String loginId) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String passwordData = rs.getString("password");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");
			return new User(idData, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User userDetail(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			int idData = Integer.parseInt(id);
			String loginIdData = rs.getString("login_id");
			String passwordData = rs.getString("password");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");
			return new User(idData, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void userUpDate(String loginId, String password, String name, String birthDate) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//確認済みのSQL
			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, loginId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void userUpDatetwo(String loginId, String name, String birthDate) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//確認済みのSQL
			String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, loginId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void userDelete(String loginId) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//確認済みのSQL
			String sql = "DELETE FROM user WHERE login_id = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String userMd(String password) {

		String re = "";

		try {

			//ハッシュを生成したい元の文字列
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			re = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(re);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re;
	}


}