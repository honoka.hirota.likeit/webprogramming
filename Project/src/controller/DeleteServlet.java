package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/*HttpSession session = request.getSession(false);

		if(session == null) {
			session = request.getSession(true);

			response.sendRedirect("LoginServlet");
		}else {
			Object loginCheck = session.getAttribute("login");

			if(loginCheck == null) {
				response.sendRedirect("LoginServlet");
			}
		}*/

		//URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User user = userDao.userDetail(id);

		request.setAttribute("user", user);

		//ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		//OKボタンで選択されたユーザに紐づくデータをユーザ情報データから削除する
		String loginId = request.getParameter("loginId");

		UserDao userDao = new UserDao();
		userDao.userDelete(loginId);

		//削除が成功したらユーザ一覧に
		response.sendRedirect("UserListServlet");

	}

}
