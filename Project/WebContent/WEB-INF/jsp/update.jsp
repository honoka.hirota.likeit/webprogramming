<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>

	<nav
		class="navbar fixed-top navbar-secondary bg-secondary justify-content-end">
		<a class="navbar-brand" href="#"></a>
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text" style="color: white">${userInfo.name}さん
			</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>


	<br>
	<br>
	<br>
	<br>

	<div align="center">

		<h2>ユーザ情報更新</h2>
		<br> <br>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert" style="color: #ff3300;">${errMsg}</div>
		</c:if>

		<form class="form-signin" action="UpdateServlet" method="post">
			<div>
				<label>ログインID : ${loginId}</label>
				<input type="hidden" name="loginId" value="${loginId}">
			</div>
			<div>
				<label>パスワード</label> <input type="password" name="password">
			</div>
			<div>
				<label>パスワード(確認)</label> <input type="password" name="confirmation">
			</div>
			<div>
				<label>ユーザー名</label>

				<input type="text" name="name" value="${name}">
			</div>
			<div>
				<label>生年月日</label>

				<input type="date" name="birthDate" value="${birthDate}">
			</div>

			<br>

			<p>
				<input type="submit" value="　  更新  　">
			</p>
		</form>

		<br>

	</div>

	<div align="left">
		<a class="btn btn-link" href="UserListServlet"> <u>戻る</u>
		</a>
	</div>

</body>

</html>