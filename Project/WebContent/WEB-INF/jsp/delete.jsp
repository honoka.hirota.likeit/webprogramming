<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
	<body>
<nav
		class="navbar fixed-top navbar-secondary bg-secondary justify-content-end">
		<a class="navbar-brand" href="#"></a>
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text" style="color: white">${userInfo.name}さん
			</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>


        <br>
            <br>
                <br>
                    <br>

        <div align="center">

        <h2>ユーザ削除確認</h2>

            <br>

 	<form class="form-signin" action="DeleteServlet" method="post">
        <div>
         <p>ログインID : ${user.loginId}<br>
          <input type="hidden" name="loginId" value="${user.loginId}">
             を本当に削除してよろしいでしょうか。</p>
            </div>

            <br>
                <br>
                    <br>

       <p><input type="submit" value="　    OK　   "></p>

	</form>

   <form class="form-signin" action="UserListServlet" method="get">
	 <p><input type="submit" value="  キャンセル  "></p>
   </form>
        </div>

</body>

</html>