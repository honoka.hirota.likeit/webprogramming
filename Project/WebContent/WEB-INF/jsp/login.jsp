<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>

<body>
	<div align="center">

		<br> <br>

		<h2>ログイン画面</h2>

		<br> <br> <br>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert" style="color: #ff3300;">${errMsg}</div>
		</c:if>

		<form class="form-signin" action="LoginServlet" method="post">
			<p>
				ログインID <input type="text" name="loginId">
			</p>

			<p>
				パスワード <input type="text" name="password">
			</p>

			<br> <br>

			<p>
				<input type="submit" value="　ログイン　">
			</p>
		</form>

	</div>

</body>

</html>
