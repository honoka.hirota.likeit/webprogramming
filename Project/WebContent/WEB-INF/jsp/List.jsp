<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
	<body>

      <nav
		class="navbar fixed-top navbar-secondary bg-secondary justify-content-end">
		<a class="navbar-brand" href="#"></a>
		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text" style="color: white">${userInfo.name}さん
			</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link" style="color: #ff3300;">ログアウト</a></li>
		</ul>
	</nav>

        <br>
            <br>
                <br>
                 	<br>

        <div align="center">

		<h2>ユーザ一覧</h2><br>

           <div align="right"><a class="btn btn-link" href = "RegistrationServlet">新規登録</a></div>

            <br>


         <form class="form-signin" action="UserListServlet" method="post">
           <div>
           <label>ログインID　　　　　　
                	<input type="text" name="loginId"></label>
            	</div>
            <div>
            <label>ユーザー名　　　　　　
                	<input type="text" name="name"></label>
            	</div>
            <div>
            <label>生年月日　　
                    <input type="date" value="年/月/日" name="birthDate1">　~　<input type="date" value="年/月/日" name="birthDate2">
                </label>
            </div>
                <div class="text-right">
                  <button type="submit" value="検索" class="btn btn-primary form-submit">検索</button>
                </div>
              </form>
            </div>

            <hr>

          <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                <tr>
                  <th>ログインID</th>
                  <th>ユーザー名</th>
                  <th>生年月日</th>
                  <th></th>
                </tr>
 			   </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>

                     <!-- TODO；ログインボタンの表示制御を行う -->
                     <td>
                       <a class="btn btn-primary" href="DetailServlet?id=${user.id}">　詳細　</a>
                       <c:if test="${userInfo.loginId=='admin' || userInfo.loginId==user.loginId}">
                       <a class="btn btn-success" href="UpdateServlet?id=${user.id}">　更新　</a>
                        </c:if>

                       <c:if test="${userInfo.loginId=='admin'}">
                       		<a class="btn btn-danger" href ="DeleteServlet?id=${user.id}">　削除　</a>
                       	</c:if>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>

</body>

</html>